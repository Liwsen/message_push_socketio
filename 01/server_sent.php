<?php
	header('Content-Type: text/event-stream');
	header('Cache-Control: no-cache');

	function imgs(){
		$imgs = array(
			'../img/01.jpg',
			'../img/02.jpg',
			'../img/03.jpg',
			'../img/04.jpg',
			'../img/05.jpg',
			'../img/06.jpg',
			'../img/07.jpg',
			'../img/08.jpg',
			'../img/09.jpg',
			'../img/10.jpg',
		);
		return $imgs[mt_rand(0,9)];
	}
	function img_content($file = null){
		$file_content = '';
		if($file !== null){
			$fp = fopen($file,"r")or die("Can't open file");  
			$file_content = chunk_split(base64_encode(fread($fp,filesize($file))));//base64编码
			fclose($fp);

			$type = getimagesize();//取得图片的大小，类型等
			switch($type[2]){//判读图片类型  
				case 1: $img_type = "gif";break;  
				case 2: $img_type = "jpg";break;  
				case 3: $img_type = "png";break;  
			}

			$file_content = str_replace(array("\r", "\n"), '', $file_content);
			
			//合成图片的base64编码
			$file_content = 'data:image/'.$type.';base64,'.$file_content;
			// $file_content = 'data:image/jpg/png/gif;base64,'.$file_content;
		}
		return $file_content;
	}

	if($img = img_content(imgs())){
		$img = '<img src="'.$img.'">';
	}else{
		$img = 'error';
	}

	//$img = '<img src="'.imgs().'">';

	echo "data: ".$img."\n\n";
	flush();
?>