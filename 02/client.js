(function () {
	var d = document,
	w = window;
	
	w.CHAT = {
		msgObj:d.getElementById("message"),
		socket:io.connect('ws://127.0.0.1:3001'),
		//推送消息内容
		submit:function(content){
			if(!!content){
				this.socket.emit('message', {content: content});
			}
			return false;
		},
		//监听消息
		listen:function(username){
			this.socket.on('message', function(obj){
				if(obj.content){
					d.getElementById('content').innerHTML = '<img src="'+ obj.content +'">';
					// d.getElementById('content').innerHTML = '<video src="http://www.w3school.com.cn/i/movie.ogg" controls="controls" autoplay></video>';
				}
			});
		}
	};
	//点击发送
	var items = d.getElementsByClassName('item');
	if(items){
		for (var i = 0; i < items.length; i++) {
			items[i].addEventListener("click", function () {
            	CHAT.submit(this.childNodes[0].attributes.src.value);
        	}, false);
		}
	}
	CHAT.listen();
})();