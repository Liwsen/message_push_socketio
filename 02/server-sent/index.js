var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.get('/', function(req, res){
	res.send('<h1>Welcome Realtime Server</h1>');
});

io.on('connection', function(socket){
	//监听推送内容
	socket.on('message', function(obj){
		//向客户端发布消息
		io.emit('message', obj);
	});
  
});

http.listen(3001, function(){
	console.log('listening on *:3001');
});